package cz.cvut.fel.ts1.cv2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Bohacr1Test {
    @Test
    public void factorialTest() {
        int n = 11;
        Bohacr1 myClass = new Bohacr1();
        long expectedResult = factorialMethod(n);
        long result = myClass.factorial(n);
        assertEquals(expectedResult, result);
    }
    public long factorialMethod(int n) {
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
